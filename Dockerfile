##Must have a wpplugin.jar in same directory
##Depends on a jdk_8 container
FROM isuper/java-oracle:jdk_8

##installazione maven e git
RUN apt-get update && apt-get install --fix-missing -y maven git

##espongo la porta 8080
EXPOSE 8080

##copio il crawler buildato nel container
COPY ./phyai-crawler.jar /

##run crawler
CMD cd /; java -Dfile.encoding=UTF-8 -jar phyai-crawler.jar --spring.datasource.url=jdbc:mysql://db:3306/phyai_crawler
